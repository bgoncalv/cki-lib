"""Unit tests for cki_pipeline.trigger_multiple()"""
import os
import unittest
from unittest import mock

import responses

from cki_lib import cki_pipeline
from cki_lib import misc
from cki_lib.gitlab import get_instance

from . import mocks


@mock.patch('cki_lib.cki_pipeline.sleep', mock.Mock())
class TestTriggerMultiple(unittest.TestCase, mocks.GitLabMocks):
    """Tests for cki_pipeline.trigger_multiple()."""

    required_variables = {'cki_project': 'cki-project',
                          'cki_pipeline_branch': 'test_branch',
                          'title': 'title'}
    required_variables_env = {'cki_pipeline_project': 'cki-pipeline',
                              'cki_pipeline_branch': 'test_branch',
                              'title': 'title'}
    multiple_variables = [{
        'cki_project': 'cki-project',
        'cki_pipeline_branch': 'branch-1',
        'title': 'title 1',
        'index': '0',
    }, {
        # 'cki_project' missing
        'cki_pipeline_branch': 'branch-2',
        'title': 'title 2',
        'index': '1',
    }, {
        'cki_project': 'cki-project',
        'cki_pipeline_branch': 'branch-3',
        'title': 'title 3',
        'index': '2',
    }]
    is_production = False

    def _trigger_multiple(self, *args, **kwargs):
        with mock.patch('cki_lib.misc.is_production',
                        mock.Mock(return_value=self.is_production)):
            return cki_pipeline.trigger_multiple(*args, **kwargs)

    def _trigger_single(self, variables, project_name):
        gitlab = get_instance('https://gitlab.com')
        self.add_project(1, project_name)
        self.add_branch(1, variables['cki_pipeline_branch'])

        self._trigger_multiple(gitlab, [variables])

        request_variables = {v['key']: v['value'] for v in self.get_requests(
            url='https://gitlab.com/api/v4/projects/1/pipeline')[0]['variables']}
        for key, value in variables.items():
            if key == 'cki_pipeline_project':
                continue
            if not self.is_production and key == 'title':
                value = f'Retrigger: {variables["title"]}'
            self.assertEqual(request_variables[key], value)
        self.assertEqual(request_variables['cki_project'], project_name)

    @responses.activate
    def test_trigger_single(self):
        """
        Test triggering a single pipeline. Only add required variables to make
        sure the code doesn't suddenly change to require something else.
        """
        self._trigger_single(self.required_variables, 'cki-project')

    @responses.activate
    @mock.patch.dict(os.environ, {'GITLAB_PARENT_PROJECT': 'cki-project'})
    def test_trigger_single_env(self):
        """
        Test triggering a single pipeline. Only add required variables to make
        sure the code doesn't suddenly change to require something else.
        """
        self._trigger_single(self.required_variables_env,
                             'cki-project/cki-pipeline')

    def _check_required_variables(self, variables, project_name, errors):
        gitlab = get_instance('https://gitlab.com')
        self.add_project(1, project_name)
        self.add_branch(1, variables['cki_pipeline_branch'])

        for expected in variables:
            # non-prod mode can cope with a missing title
            if not self.is_production and expected == 'title':
                continue
            missing = variables.copy()
            del missing[expected]
            with self.assertRaises(errors):
                self._trigger_multiple(gitlab, [missing])

    @responses.activate
    def test_required_variables(self):
        """
        Verify the expected variables are still required to trigger pipelines
        successfully.
        """
        self._check_required_variables(self.required_variables,
                                       'cki-project',
                                       (KeyError, misc.EnvVarNotSetError))

    @responses.activate
    @mock.patch.dict(os.environ, {'GITLAB_PARENT_PROJECT': 'cki-project'})
    def test_required_variables_env(self):
        """
        Verify the expected variables are still required to trigger pipelines
        successfully.
        """
        self._check_required_variables(self.required_variables_env,
                                       'cki-project/cki-pipeline',
                                       KeyError)

    @responses.activate
    def test_trigger_multiple_errors(self):
        """
        Test triggering multiple pipelines, one of which is invalid.

        All pipelines should be tried, in spite of the pipeline in the middle being invalid,
        from missing the cki_project variable.
        """
        variables = self.multiple_variables

        gitlab = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        for pipeline_variables in self.multiple_variables:
            self.add_branch(1, pipeline_variables['cki_pipeline_branch'])

        with (
            self.assertRaisesRegex(KeyError, r"cki_project"),
            self.assertLogs(logger=cki_pipeline.LOGGER, level="ERROR") as log_ctx,
        ):
            self._trigger_multiple(gitlab, variables)

        self.assertIn(
            f"ERROR:{cki_pipeline.LOGGER.name}:Pipeline 2/3 for branch-2 could not be triggered",
            "\n".join(log_ctx.output).splitlines(),
        )

        pipeline_requests = self.get_requests(
            url='https://gitlab.com/api/v4/projects/1/pipeline')

        self.assertEqual(len(pipeline_requests), 2)
        request_variables = {v['key']: v['value'] for v in pipeline_requests[0]['variables']}
        self.assertEqual(request_variables['index'], variables[0]['index'])
        request_variables = {v['key']: v['value'] for v in pipeline_requests[1]['variables']}
        self.assertEqual(request_variables['index'], variables[2]['index'])

    @responses.activate
    def test_returned_no_errors(self):
        """
        Verify the function returns a list of triggered pipelines if no errors
        were encountered when triggering.
        """
        gitlab = get_instance('https://gitlab.com')
        self.add_project(1, 'cki-project')
        for pipeline_variables in self.multiple_variables:
            self.add_branch(1, pipeline_variables['cki_pipeline_branch'])

        triggers = self.multiple_variables.copy()
        del triggers[1]  # Remove the trigger that's causing exception

        triggered_pipelines = self._trigger_multiple(gitlab, triggers)
        self.assertEqual(len(triggered_pipelines), 2)


class TestTriggerMultipleProduction(TestTriggerMultiple):
    """Tests for cki_pipeline.trigger_multiple() with is_production = true."""
    required_variables = TestTriggerMultiple.required_variables.copy()
    required_variables.update({'title': 'commit_title'})
    required_variables_env = TestTriggerMultiple.required_variables_env.copy()
    required_variables_env.update({'title': 'commit_title'})
    is_production = True
