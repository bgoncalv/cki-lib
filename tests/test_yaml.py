"""Yaml util tests."""
import contextlib
from importlib import resources
import io
import pathlib
import sys
import tempfile
import typing
import unittest

import responses

from cki_lib import yaml

from .assets import yaml as assets


class TestYaml(unittest.TestCase):
    """Test yaml utils."""

    def test_list(self) -> None:
        """Check lint indentation."""
        self.assertEqual(yaml.dump({"key": ["value"]}), "---\nkey:\n  - value\n")

    def test_normal_string(self) -> None:
        """Check block string dumping without line feeds."""
        self.assertEqual(yaml.dump({"key": "value"}), "---\nkey: value\n")

    def test_line_feed(self) -> None:
        """Check block string dumping with line feeds."""
        self.assertEqual(yaml.dump({"key": "value\nline 2"}),
                         "---\nkey: |-\n  value\n  line 2\n")

    def test_valid_data(self) -> None:
        """Check roundtripping some data."""
        data = {"a": "b",
                "c": ["d", {"e": "f\ng", "h": "i", "k\nl": ["m"]}]}
        self.assertEqual(data, yaml.load(contents=yaml.dump(data)))

    def test_read_yaml_file(self) -> None:
        """Check reading YAML files."""
        with tempfile.NamedTemporaryFile() as yaml_file:
            yaml_file.write(b'pooh: bear')
            yaml_file.flush()
            self.assertEqual(yaml.load(file_path=yaml_file.name),
                             {'pooh': 'bear'})

    def test_read_yaml_resources(self) -> None:
        """Check reading YAML resources."""
        # Python 3.12 will allow to use resources.files() / 'assets/yaml/test.json'
        self.assertEqual(yaml.load(file_path=resources.files(assets) / 'test.json'),
                         {'pooh': 'bear'})

    def test_schema(self) -> None:
        """Check validating YAML resources while loading."""
        cases = (
            ('valid', 'object-schema.yml', True),
            ('invalid', 'string-schema.yml', False),
        )
        for description, schema, expected in cases:
            with self.subTest(description):
                with contextlib.nullcontext() if expected else self.assertRaises(Exception):
                    yaml.load(file_path=resources.files(assets) / 'test.json',
                              schema_path=resources.files(assets) / schema)

    def test_config_tree_schema(self) -> None:
        """Check validating YAML resources while loading."""
        cases = (
            ('valid', 'config-schema.yml', True),
            ('invalid', 'string-schema.yml', False),
        )
        for description, schema, expected in cases:
            with self.subTest(description):
                with contextlib.nullcontext() if expected else self.assertRaises(Exception):
                    yaml.load(file_path=resources.files(assets) / 'test.yml',
                              schema_path=resources.files(assets) / schema,
                              process_config_tree=True)

    def test_read_yaml_contents(self) -> None:
        """Check reading YAML from a string."""
        self.assertEqual(yaml.load(contents='pooh: bear'), {'pooh': 'bear'})
        self.assertEqual(yaml.load(contents='{}'), {})
        self.assertEqual(yaml.load(), None)

    def test_resolve_reference_dict(self) -> None:
        """Check resolving references."""
        contents = 'pooh: {bear: bar}\ncrow: !reference [pooh, bear]'
        self.assertEqual(yaml.load(contents=contents, resolve_references=True),
                         {'pooh': {'bear': 'bar'}, 'crow': 'bar'})

    def test_resolve_reference_list(self) -> None:
        """Check resolving references."""
        contents = 'pooh: {bear: bar}\ncrow: [!reference [pooh, bear]]'
        self.assertEqual(yaml.load(contents=contents, resolve_references=True),
                         {'pooh': {'bear': 'bar'}, 'crow': ['bar']})

    def test_resolve_reference_no_dict(self) -> None:
        """Check resolving references doesn't work for non-dict roots."""
        contents = '[pooh, bear: !reference [0]]'
        self.assertRaises(Exception,
                          lambda: yaml.load(contents=contents, resolve_references=True))

    def test_include_missing(self) -> None:
        """Check .include is not required."""
        contents = 'pooh: {bear: bar}'
        self.assertEqual(yaml.load(contents=contents, resolve_includes=True),
                         {'pooh': {'bear': 'bar'}})

    def test_include_non_dict(self) -> None:
        """Check non-dicts and .include support don't collide."""
        self.assertEqual(yaml.load(contents='[pooh, {bear: bar}]', resolve_includes=True),
                         ['pooh', {'bear': 'bar'}])
        self.assertEqual(yaml.load(contents='pooh', resolve_includes=True),
                         'pooh')

    @responses.activate
    def test_include_url(self) -> None:
        """Check resolving url includes."""
        contents = '.include: https://host/bear\npooh: {bear: bar}'
        responses.add(responses.GET, 'https://host/bear',
                      json={'pooh': {'bear': 'brrr', 'crow': 'fred'}})
        self.assertEqual(yaml.load(contents=contents, resolve_includes=True),
                         {'pooh': {'bear': 'bar', 'crow': 'fred'}})

    def test_include_file(self) -> None:
        """Check resolving url includes."""
        with tempfile.TemporaryDirectory() as directory:
            pathlib.Path(directory, 'main.yml').write_text('.include: [inc.yml]\npooh: {bear: bar}')
            pathlib.Path(directory, 'inc.yml').write_text('pooh: {bear: brrr, crow: fred}')
            self.assertEqual(yaml.load(file_path=f'{directory}/main.yml',
                                       resolve_includes=True),
                             {'pooh': {'bear': 'bar', 'crow': 'fred'}})

    def test_include_no_list(self) -> None:
        """Check resolving includes doesn't work for weird .include."""
        contents = '.include: {pooh: bear}'
        self.assertRaises(Exception,
                          lambda: yaml.load(contents=contents, resolve_includes=True))

    def test_validate(self) -> None:
        """Test behavior of yaml.validate."""
        cases = (
            ('passes', 'pooh', True),
            ('invalid', 1, False),
        )
        schema = {'type': 'string'}
        for description, contents, expected_result in cases:
            with self.subTest(description):
                if expected_result:
                    self.assertEqual(yaml.validate(contents, schema), contents)
                else:
                    self.assertRaises(yaml.ValidationError, yaml.validate, contents, schema)

    @staticmethod
    @contextlib.contextmanager
    def redirect_stdin(new: str) -> typing.Iterator[None]:
        """Redirect stdin."""
        old_stream = sys.stdin
        sys.stdin = io.StringIO(new)
        try:
            yield
        finally:
            sys.stdin = old_stream

    def test_main_validation_cki_schema(self) -> None:
        """Test validation behavior of yaml.main with $cki* statements."""
        cases = (
            ('default', ['a: {pooh: bear}'], 'cki-schema.yml', 0),
            ('invalid', ['a: {crow: bar}'], 'cki-schema.yml', 1),
            ('.default', ['.default: {pooh: bear}\na: {}'],
             'cki-schema-process-config-tree.yml', 0),
            ('.extends', ['.b: {pooh: bear}\na: {.extends: .b}'],
             'cki-schema-process-config-tree.yml', 0),
            ('reference', ['a: {crow: bar, pooh: !reference [a, crow]}'],
             'cki-schema-resolve-references.yml', 0),
            ('.include', ['.include: data-1.yml\na: {}', 'a: {pooh: bear}'],
             'cki-schema-resolve-includes.yml', 0),
        )
        for description, files, schema, expected_result in cases:
            with self.subTest(description), tempfile.TemporaryDirectory() as temp_directory:
                file_names = []
                for index, contents in enumerate(files):
                    file_path = pathlib.Path(temp_directory, f'data-{index}.yml')
                    file_path.write_text(f'.schema: tests.assets.yaml/{schema}\n' + contents,
                                         encoding='utf8')
                    file_names.append(str(file_path))
                with self.assertRaises(SystemExit) as e:
                    yaml.main(['validate', file_names[0]])
                self.assertEqual(e.exception.code, expected_result)

    def test_main_validation(self) -> None:
        """Test validation behavior of yaml.main."""
        cases = (
            ('passes', [{'a': {'pooh': 'bear'}}], None, [], 0),
            ('multiple passing', [{'a': {'pooh': 'bear'}}, {'b': {'pooh': 'bar'}}], None, [], 0),
            ('invalid', [1], None, [], 1),
            ('first invalid', [1, {'a': {'pooh': 'bear'}}], None, [], 1),
            ('second invalid', [{'a': {'pooh': 'bear'}}, 1], None, [], 1),
            ('stdin', [], 'a: {pooh: bear}', [], 0),
            ('stdin invalid', [], '1', [], 1),
            ('.default', [{'.default': {'pooh': 'bear'}, 'a': {}}], None,
             ['--process-config-tree'], 0),
            ('.extends', [{'.b': {'pooh': 'bear'}, 'a': {'.extends': '.b'}}], None,
             ['--process-config-tree'], 0),
            ('reference', [], 'a: {crow: bar, pooh: !reference [a, crow]}',
             ['--resolve-references'], 0),
            ('.include', [{'a': {'pooh': 'bear'}}, {'.include': 'data-0.yml', 'a': {}}], None,
             ['--resolve-includes'], 0),
        )
        schema = {
            'type': 'object',
            'additionalProperties': {
                'type': 'object',
                'properties': {'pooh': {'type': 'string'}},
                'required': ['pooh'],
            }
        }
        for description, contents, stdin, additional_args, expected in cases:
            with self.subTest(description), tempfile.TemporaryDirectory() as temp_directory:
                schema_path = pathlib.Path(temp_directory, 'schema.yml')
                schema_path.write_text(yaml.dump(schema), encoding='utf8')
                file_names = []
                for index, yaml_contents in enumerate(contents):
                    file_path = pathlib.Path(temp_directory, f'data-{index}.yml')
                    file_path.write_text(yaml.dump(yaml_contents), encoding='utf8')
                    file_names.append(str(file_path))
                args = ['validate', '--schema', str(schema_path)] + additional_args + file_names
                with self.assertRaises(SystemExit) as e, self.redirect_stdin(stdin):
                    yaml.main(args)
                self.assertEqual(e.exception.code, expected)

    def test_main(self):
        """Test validation behavior of yaml.main."""
        cases = (
            ('set', ['set-value', 'pooh.1.bear', 'zig'],
             {'pooh': ['crow', {'bear': 'bar'}]},
             {'pooh': ['crow', {'bear': 'zig'}]}),
            ('set with lists', ['set-value', 'pooh.1', 'zig'],
             {'pooh': ['bear', 'bar']},
             {'pooh': ['bear', 'zig']}),
            ('set with dots', ['set-value', r'pooh\.crow.1', 'zig'],
             {'pooh.crow': ['bear', 'bar']},
             {'pooh.crow': ['bear', 'zig']}),
            ('del', ['del', 'pooh.bear'],
             {'pooh': {'bear': 'bar', 'crow': 'zig'}},
             {'pooh': {'crow': 'zig'}}),
            ('del with lists', ['del', 'pooh.0'],
             {'pooh': ['bear', 'bar']},
             {'pooh': ['bar']}),
            ('dump', ['dump'],
             {'pooh': ['bear', 'bar']},
             {'pooh': ['bear', 'bar']}),
        )
        for description, args, data, expected in cases:
            with (
                self.subTest(description),
                contextlib.redirect_stdout(io.StringIO()) as stdout,
                self.redirect_stdin(yaml.dump(data)),
            ):
                yaml.main(args)
            self.assertEqual(yaml.load(contents=stdout.getvalue()), expected)

    def test_format(self) -> None:
        """Test behavior of --format."""
        cases = (
            ('yaml', '---\n- pooh\n- bear\n'),
            ('json', '["pooh", "bear"]'),
        )
        for output_format, expected in cases:
            with (
                self.subTest(output_format),
                contextlib.redirect_stdout(io.StringIO()) as stdout,
                self.redirect_stdin(yaml.dump(['pooh', 'bear'])),
            ):
                yaml.main(['dump', '--format', output_format])
                self.assertEqual(stdout.getvalue(), expected)
