"""Tests for config_tree."""

import unittest

from cki_lib import config_tree


class TestProcessConfigTree(unittest.TestCase):
    """Tests for patch_trigger.process_config_tree()."""

    def test_removal(self):
        """Test removal of keys with a leading dot."""
        config = {
            'one': {'two': 'three'},
            '.four': {'five': 'six'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'two': 'three'}})

    def test_extends(self):
        """Test extending one level deep."""
        config = {
            'one': {'.extends': '.four', 'two': 'three'},
            '.four': {'five': 'six'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'two': 'three', 'five': 'six'}})

    def test_extends_multilevel(self):
        """Test extending multiple levels deep."""
        config = {
            'one': {'.extends': '.two'},
            '.two': {'.extends': '.three'},
            '.three': {'.extends': '.four'},
            '.four': {'five': 'six'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'five': 'six'}})

    def test_extends_mixins(self):
        """Test extending from multiple base configs."""
        config = {
            'one': {'.extends': ['.two', '.three', '.four']},
            '.two': {'two': 'five'},
            '.three': {'two': 'six'},
            '.four': {'three': 'seven'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'two': 'six', 'three': 'seven'}})

    def test_extends_non_dot(self):
        """Test extending one level deep from a non-dot config."""
        config = {
            'one': {'.extends': 'four', 'two': 'three'},
            'four': {'five': 'six'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'two': 'three', 'five': 'six'}, 'four': {'five': 'six'}})

    def test_extends_override(self):
        """Test extending one level deep with overriding."""
        config = {
            'one': {'.extends': '.four', 'two': 'three'},
            '.four': {'two': 'six', 'five': 'seven'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'two': 'three', 'five': 'seven'}})

    def test_extends_error(self):
        """Test extending a non-existent base config."""
        config = {
            'one': {'.extends': '.four'},
        }
        self.assertRaises(Exception, lambda: config_tree.process_config_tree(config))

    def test_extends_default(self):
        """Test inheriting from .default."""
        config = {
            '.default': {'seven': 'eight'},
            'one': {'two': 'three'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'two': 'three', 'seven': 'eight'}})

    def test_extends_default_extends(self):
        """Test inheriting from .default with a .extends clause."""
        config = {
            '.default': {'seven': 'eight'},
            'one': {'.extends': '.four', 'two': 'three'},
            '.four': {'five': 'six'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'two': 'three', 'five': 'six', 'seven': 'eight'}})

    def test_extends_default_override(self):
        """Test inheriting from .default with overriding."""
        config = {
            '.default': {'two': 'seven'},
            'one': {'.extends': '.four', 'two': 'three'},
            '.four': {'five': 'six'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'two': 'three', 'five': 'six'}})

    def test_extends_default_override_indirect(self):
        """Test inheriting from .default with indirect overriding."""
        config = {
            '.default': {'five': 'seven'},
            'one': {'.extends': '.four', 'two': 'three'},
            '.four': {'five': 'six'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'two': 'three', 'five': 'six'}})

    def test_extends_default_not_extends(self):
        """Test that .default is not used during .extends."""
        config = {
            '.default': {'one': 'two'},
            'one': {'.extends': ['.three', '.four']},
            '.three': {'one': 'five'},
            '.four': {'six': 'seven'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'one': 'five', 'six': 'seven'}})

    def test_extends_default_extends_default(self):
        """Test that .default can use .extends."""
        config = {
            '.default': {'.extends': '.three', 'two': 'one'},
            'one': {'.extends': ['.four']},
            '.three': {'one': 'five'},
            '.four': {'six': 'seven'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'one': 'five', 'two': 'one', 'six': 'seven'}})

    def test_clean(self):
        """Test that .stuff is removed during cleaning."""
        config = {'one': 'two', '.two': 'three', 'four': [{'.five': 'six'}]}
        self.assertEqual(config_tree.clean_config(config),
                         {'one': 'two', 'four': [{}]})

    def test_merge_deep(self):
        """Test that dict merging works at the deeper levels."""
        config = {
            'one': {'.extends': ['.two'], 'three': {'four': {'five': 'six'}}},
            '.two': {'three': {'four': {'seven': 'eight'}}},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'three': {'four': {'five': 'six', 'seven': 'eight'}}}})

    def test_merge_override(self):
        """Test that dict merging overrides keys."""
        config = {
            'one': {'.extends': ['.two'], 'three': {'four': {'five': 'six'}}},
            '.two': {'three': {'four': {'five': 'seven'}}},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'three': {'four': {'five': 'six'}}}})

    def test_merge_one_level(self):
        """Test that dict merging works at the top level."""
        config = {
            'one': {'.extends': ['.two'], 'three': {'four': 'five'}},
            '.two': {'three': {'six': 'seven'}},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'three': {'four': 'five', 'six': 'seven'}}})

    def test_merge_array(self):
        """Test that merging overrides arrays."""
        config = {
            'one': {'.extends': ['.two'], 'three': ['four', 'five']},
            '.two': {'three': ['six', 'seven']},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'three': ['four', 'five']}})

    def test_merge_dicts(self):
        """Test that dictionaries are correcly merged."""
        config = {'one': 'two', 'two': {'three': 'five'}}
        new = {'one': 'four', 'two': {'five': 'six'}, 'three': 'six'}
        self.assertEqual(config_tree.merge_dicts(config, new), {
            'one': 'four', 'three': 'six',
            'two': {'three': 'five', 'five': 'six'}})

    def test_null(self):
        """Test null top-level values are handled correctly."""
        config = {
            'one': None,
            '.default': {'two': 'three'},
        }
        self.assertEqual(config_tree.process_config_tree(config), {
            'one': {'two': 'three'}})
