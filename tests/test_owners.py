"""Test cki_lib.owners."""
from importlib import resources
import unittest

from cki_lib import owners
from cki_lib import yaml

from . import assets


class TestOwners(unittest.TestCase):
    """Test cki_lib.owners."""

    def test_empty_parser(self):
        """Test the behavior of an empty owners.yaml file."""
        parser = owners.Parser(None)
        self.assertEqual(len(parser.subsystems), 0)

        config = yaml.load(contents=(resources.files(assets) /
                           'owners/invalid.yaml').read_text('utf8'))
        parser = owners.Parser(config)
        self.assertEqual(len(parser.subsystems), 1)

        subsystem = parser.subsystems[0]
        self.assertEqual(subsystem.subsystem_name, '')
        self.assertEqual(subsystem.subsystem_label, '')
        self.assertEqual(subsystem.ready_for_merge_label_deps, [])
        self.assertEqual(subsystem.status, '')
        self.assertEqual(subsystem.required_approvals, False)
        self.assertEqual(subsystem.maintainers, [])
        self.assertEqual(subsystem.reviewers, [])
        self.assertEqual(subsystem.scm, '')
        self.assertEqual(subsystem.mailing_list, '')
        self.assertEqual(subsystem.jira_component, '')
        self.assertEqual(subsystem.test_variants, [])
        self.assertEqual(subsystem.path_include_regexes, [])
        self.assertEqual(subsystem.path_includes, [])
        self.assertEqual(subsystem.path_excludes, [])
        self.assertFalse(subsystem.matches(['makefile']))

    def test_parser(self):
        """Test the behavior of the parser."""
        config = yaml.load(contents=(resources.files(assets) /
                           'owners/owners.yaml').read_text('utf8'))
        parser = owners.Parser(config)
        subsystems = parser.subsystems
        self.assertEqual(len(subsystems), 2)

        subsystem = subsystems[1]
        self.assertEqual(subsystem.subsystem_name, 'Some Subsystem')
        self.assertEqual(subsystem.subsystem_label, 'redhat')
        self.assertEqual(subsystem.ready_for_merge_label_deps, ['testDep'])
        self.assertEqual(subsystem.status, 'Maintained')
        self.assertTrue(subsystem.required_approvals)
        self.assertEqual(subsystem.maintainers, [{'name': 'User 1', 'email': 'user1@redhat.com'},
                                                 {'name': 'User 2', 'email': 'user2@redhat.com'}])
        self.assertEqual(subsystem.reviewers, [{'name': 'User 0', 'email': 'user0@redhat.com'},
                                               {'name': 'User 3', 'email': 'user3@redhat.com'},
                                               {'name': 'User 4', 'email': 'user4@redhat.com',
                                                'restricted': True}])

        self.assertEqual(subsystem.scm, 'git https://gitlab.com/cki-project/kernel-ark.git')
        self.assertEqual(subsystem.mailing_list, 'https://somelist.redhat.com/xxx/')
        self.assertEqual(subsystem.jira_component, 'kernel / misc')
        self.assertEqual(subsystem.path_include_regexes, ['bpf'])
        self.assertEqual(subsystem.path_includes, ['makefile', 'redhat/'])
        self.assertEqual(subsystem.path_excludes, ['redhat/configs/'])
        self.assertEqual(str(subsystem), 'Some Subsystem')
        self.assertEqual(subsystem.devel_sst, ['rhel-sst-something'])
        self.assertEqual(subsystem.qe_sst, ['rhel-sst-somethingelse'])
        self.assertEqual(subsystem.test_variants, ['kernel-rt'])

        self.assertTrue(subsystem.matches(['makefile']))
        self.assertTrue(subsystem.matches(['redhat/']))
        self.assertTrue(subsystem.matches(['redhat/Makefile']))
        self.assertTrue(subsystem.matches(['include/linux/bpf.h']))
        self.assertFalse(subsystem.matches(['redhat/configs/generic/CONFIG_NET']))
        self.assertFalse(subsystem.matches(['drivers/iio/light/tsl2772.c']))
        self.assertTrue(subsystem.matches(['makefile', 'redhat/configs/generic/CONFIG_NET']))

        self.assertEqual(len(parser.get_matching_subsystems(['makefile'])), 1)
        self.assertEqual(len(parser.get_matching_subsystems(['redhat/'])), 1)
        self.assertEqual(len(parser.get_matching_subsystems(['redhat/Makefile'])), 1)
        self.assertEqual(len(parser.get_matching_subsystems(
            ['redhat/configs/generic/CONFIG_NET'])), 0)
        self.assertEqual(len(parser.get_matching_subsystems(['drivers/iio/light/tsl2772.c'])), 0)

        # pylint: disable=protected-access
        self.assertEqual(parser.get_matching_subsystems_by_label('redhat'), [parser.subsystems[1]])
        self.assertEqual(parser.get_matching_subsystem_by_name('Some Subsystem'),
                         parser.subsystems[1])

    def test_parser_null_subsystems(self):
        """Null values for maintainers or reviewers in the yaml should return an empty list."""
        config = yaml.load(contents=(resources.files(assets) /
                           'owners/no-maintainers.yaml').read_text('utf8'))
        parser = owners.Parser(config)
        subsystem = parser.subsystems[0]
        self.assertEqual(subsystem.maintainers, [])
        self.assertEqual(subsystem.reviewers, [])
        self.assertEqual(subsystem.ready_for_merge_label_deps, [])

    def test_glob_match(self):
        """Test file name matching."""
        cases = (
            ('usr/*', False),
            ('usr/bin/*', True),
            ('usr/bin/f[ab]*', True),
            ('usr/bin/f[xy]*', False),
            ('usr/bin/*al*', True),
            ('usr/bin/false', True),
            ('usr/*/false', True),
            ('usr/???/false', True),
            ('usr/????/false', False),
            ('*', False),
            ('*/*', False),
            ('*/*/*', True),
            ('usr/', True),
            ('usr/bin/', True),
            ('usr/sbin/', False),
            ('*/', True),
            ('usr/bin', False),
        )
        for pattern, expected_result in cases:
            with self.subTest(pattern):
                self.assertEqual(owners.glob_match('usr/bin/false', pattern), expected_result)

    def test_main(self):
        """Test the main method."""
        config = (resources.files(assets) / 'owners/main.yaml').read_text('utf8')
        cases = (
            (['--', 'drivers/iio/light/tsl2772.c'], ''),
            (['--', 'no-label-file'], ''),
            (['--', 'label-file'], '@label\n@label/label-file\n'),
            (['--', 'duplicate-label-file'],
             '@duplicate-label\n@duplicate-label/duplicate-label-file\n'),
            (['--', 'label-file', 'duplicate-label-file'],
             '@duplicate-label\n@duplicate-label/duplicate-label-file\n'
             '@label\n@label/label-file\n'),
            (['--file-list', str(resources.files(assets) / 'owners/file-list.txt')],
             '@duplicate-label\n@duplicate-label/duplicate-label-file\n'
             '@label\n@label/label-file\n'),
            (['--', '--dash-file'], ''),
            (['--output', 'json', '--', 'label-file', 'duplicate-label-file'],
             (resources.files(assets) / 'owners/json-output.json').read_text('utf8')),
        )
        for args, expected in cases:
            with self.subTest(args):
                self.assertEqual(owners.main(['--owners-yaml', config] + args), expected)
