"""Misc api interaction tests."""
import argparse
from datetime import datetime
from datetime import timedelta
from datetime import timezone
import os
import subprocess
import sys
import unittest
from unittest import mock

from freezegun import freeze_time

from cki_lib import misc


class TestMisc(unittest.TestCase):
    """Test misc class."""

    def test_log_exceptions(self):
        """Check that exceptions are logged."""
        with self.assertLogs(misc.LOGGER, 'ERROR'):
            with misc.only_log_exceptions():
                raise Exception('test')

    @staticmethod
    def _raise_exception():
        with misc.only_log_exceptions(RuntimeError):
            raise Exception('test')

    def test_log_exceptions_no_match(self):
        """Check that unfiltered exceptions get through."""
        self.assertRaises(Exception, self._raise_exception)

    def test_flattened(self):
        """Check behavior of misc.flattened()."""
        cases = (
            ([], []),
            (['foo'], ['foo']),
            ([['foo']], ['foo']),
            ([['foo'], 'bar'], ['foo', 'bar']),
            ([['foo', 'bar']], ['foo', 'bar']),
            ([['foo', 'bar'], 'baz'], ['foo', 'bar', 'baz']),
            (['foo', 'bar', 'baz'], ['foo', 'bar', 'baz']),
            ('foo', ['foo']),
        )
        for lst, expected in cases:
            with self.subTest(lst):
                self.assertEqual(list(misc.flattened(lst)), expected)

    def test_get_nested_key(self):
        """Test get_nested_key."""
        test_dict = {'a': {'b': {'c': 'value', 'd': False, 'e': None},
                           'f': ['g', 'h'], 'i': {'0': 'j'}},
                     'h': mock.Mock(i={'j': 'k'})}
        self.assertEqual(misc.get_nested_key(test_dict, 'a/b/c'), 'value')
        self.assertIsNone(misc.get_nested_key(test_dict, 'a/missing/missing2'))
        self.assertEqual(misc.get_nested_key(test_dict, 'a/b/d'), False)
        self.assertEqual(misc.get_nested_key(test_dict, 'a/b/e'), None)
        self.assertEqual(misc.get_nested_key(test_dict, 'a/b/e/f'), None)
        self.assertEqual(misc.get_nested_key(test_dict, 'a/f/0'), 'g')
        self.assertEqual(misc.get_nested_key(test_dict, 'a/f/2'), None)
        self.assertEqual(misc.get_nested_key(test_dict, 'a/f/foo'), None)
        self.assertEqual(misc.get_nested_key(test_dict, 'a/i/0'), 'j')

        self.assertEqual(misc.get_nested_key(test_dict, 'h/i/j'), None)
        self.assertEqual(misc.get_nested_key(test_dict, 'h/i/j', lookup_attrs=True), 'k')
        self.assertEqual(misc.get_nested_key(test_dict, 'h/i/l', lookup_attrs=True), None)

        self.assertEqual(misc.get_nested_key(test_dict, 'a.b.c', delimiter='.'), 'value')

    def test_set_nested_key(self):
        """Test set_nested_key."""
        cases = [
            ('a/b/c', {'a': {}}, {'a': {'b': {'c': 'foo'}}}),
            ('c',     {}, {'c': 'foo'}),
            ('c/b/c', {}, {'c': {'b': {'c': 'foo'}}}),
            ('b/c', {'b': 'string'}, None),
            ('1/c', ['a', {'c': 'string'}], ['a', {'c': 'foo'}]),
            ('a/1', {'a': ['b', 'c']}, {'a': ['b', 'foo']}),
        ]

        for key, value, expected in cases:
            with self.subTest(key=key):
                if expected is None:
                    with self.assertRaises(Exception):
                        misc.set_nested_key(value, key, 'foo')
                else:
                    misc.set_nested_key(value, key, 'foo')
                    self.assertEqual(value, expected)

    def test_append_to_nested_list_where_key_does_not_exist(self):
        """Test append_to_nested_list when key does not exist."""
        cases = [
            ('a/b/c',
             {'foo': 'bar'},
             {'a': {'b': {'c': [{'foo': 'bar'}]}}, 'b': 'string'}
             ),
            ('c',
             {'foo': 'bar'},
             {'a': {},
              'b': 'string',
              'c': [{'foo': 'bar'}]}
             ),
            ('c/b/c',
             {'foo': 'bar'},
             {'a': {}, 'c': {'b': {'c': [{'foo': 'bar'}]}}, 'b': 'string'}
             ),
        ]

        for key, value, expected in cases:
            with self.subTest(key=key):
                data = {'a': {}, 'b': 'string'}
                misc.append_to_nested_list(data, key, value)
                self.assertDictEqual(data, expected, key)

    def test_append_to_nested_list_where_key_exists_and_its_values_is_a_list(self):
        """Test append_to_nested_list when key exists and its value is a list."""
        cases = [
            ('a/b/c',
             {'foo': 'bar'},
             {'a': {'b': {'c': [{'bar': 'foo'}]}}, 'b': 'string'},
             {'a': {'b': {'c': [{'bar': 'foo'}, {'foo': 'bar'}]}}, 'b': 'string'}
             ),
            ('c',
             {'foo': 'bar'},
             {'a': {}, 'c': [{'bar': 'foo'}], 'b': 'string'},
             {'a': {},
              'b': 'string',
              'c': [{'bar': 'foo'}, {'foo': 'bar'}]}
             ),
            ('c/b/c',
             {'foo': 'bar'},
             {'a': {}, 'c': {'b': {'c': [{'bar': 'foo'}]}}, 'b': 'string'},
             {'a': {}, 'c': {'b': {'c': [{'bar': 'foo'}, {'foo': 'bar'}]}}, 'b': 'string'}
             ),
        ]

        for key, value, data, expected in cases:
            with self.subTest(key=key):
                misc.append_to_nested_list(data, key, value)
                self.assertDictEqual(data, expected, key)

    def test_append_to_nested_list_where_key_exists_and_its_values_is_not_a_list(self):
        """Test append_to_nested_list when key exists and its value is not a list."""
        with self.assertRaisesRegex(Exception, 'Element is not a list'):
            data = {'a': {}, 'b': 'string'}
            misc.append_to_nested_list(data, 'b', {'foo': 'bar'})

    def test_get_env_var(self):
        """Ensure get_env_var_or_raise works."""
        with self.assertRaises(misc.EnvVarNotSetError):
            misc.get_env_var_or_raise('HOLY BULL IS REAL')

        key = None
        # select random first key from env
        for k in os.environ:
            key = k
            break

        # the function must return the same value as os.environ
        self.assertEqual(os.environ[key], misc.get_env_var_or_raise(key))

    def test_tempfile_from_string(self):
        """Ensure tempfile_from_string works."""
        test_data = b'a horse, or a cabbage'
        with misc.tempfile_from_string(test_data) as fname:
            self.assertTrue(os.path.isfile(fname))

            with open(fname, 'rb') as fobj:
                self.assertEqual(fobj.read(), test_data)

    def test_enter_dir(self):
        """Ensure enter_dir works."""
        current = os.getcwd()
        with misc.enter_dir('/'):
            self.assertEqual(os.getcwd(), '/')

        self.assertEqual(os.getcwd(), current)

    def test_safe_popen(self):
        """Ensure safe_popen works."""

        def fake_popen(*args, **kwargs):
            # pylint: disable=unused-argument
            fake_popen.communicate = lambda *a, **kw: (b'stdout data',
                                                       b'stderr data')
            fake_popen.returncode = 0
            fake_popen.called = True
            return fake_popen

        with mock.patch('subprocess.Popen.__enter__', new_callable=fake_popen) as mpopen:
            stdout, stderr, retcode = misc.safe_popen(['echo', 'stdout data'],
                                                      stdout=subprocess.PIPE,
                                                      stderr=subprocess.PIPE)

            self.assertTrue(mpopen.called)
            self.assertEqual(stdout, 'stdout data')
            self.assertEqual(stderr, 'stderr data')
            self.assertEqual(retcode, 0)

    def test_retry_safe_popen(self):
        """Ensure retry_safe_popen works."""

        def fake_warn(*args, **kwargs):
            # pylint: disable=unused-argument
            assert ('HOLLY BULL' in arg or 'RETRY' in arg for arg in args)
            return fake_warn

        with mock.patch('logging.warning', fake_warn):
            with mock.patch('time.sleep', lambda x: x):
                with self.assertRaises(RuntimeError):
                    pycmd = "import sys; sys.stderr.write('HOLLY BULL')"
                    misc.retry_safe_popen(['HOLLY BULL'], ['python3', '-c',
                                                           pycmd],
                                          stderr=subprocess.PIPE)

        misc.retry_safe_popen([''], ['echo', 'duh'], stdout=subprocess.PIPE)

    def test_read_stream(self):
        """Ensure read_stream works and does not block."""
        proc = subprocess.Popen(['echo', 'MAGIC'], stdout=subprocess.PIPE)

        result = ''
        while not result:
            result = misc.read_stream(proc.stdout)

        self.assertEqual('MAGIC', result.strip())

    @freeze_time("2010-01-02 00:00:00")
    def test_utc_now_iso(self):
        """Test utc_now_iso."""
        self.assertEqual(misc.utc_now_iso(), "2010-01-02T00:00:00+00:00")

    def test_now_tz_utc(self):
        """Test now_tz_utc."""
        cases = (
            '2010-01-02 00:00:00+00:00',
            '2010-01-02 01:00:00+01:00',
        )
        for dt in cases:
            with self.subTest(dt), freeze_time(dt):
                self.assertEqual(misc.now_tz_utc().isoformat(),
                                 datetime(2010, 1, 2, tzinfo=timezone.utc).isoformat())

    def test_datetime_fromisoformat_tz_utc(self):
        """Test datetime_fromisoformat_tz_utc."""
        cases = (
            '2010-01-02',
            '2010-01-02 00:00:00',
            '2010-01-02 00:00:00Z',
            '2010-01-02 00:00:00+00:00',
            '2010-01-02 01:00:00+01:00',
            # cki_tools.webhook_receiver.receiver feeds this func a HTTP header date!
            'Sat, 02 Jan 2010 00:00:00 GMT'
        )
        for dt in cases:
            with self.subTest(dt):
                self.assertEqual(misc.datetime_fromisoformat_tz_utc(dt).isoformat(),
                                 datetime(2010, 1, 2, tzinfo=timezone.utc).isoformat())

    def test_ensure_tz_utc(self):
        """Test ensure_tz_utc."""
        cases = (
            datetime(2010, 1, 2, 0, 0, 0, tzinfo=None),
            datetime(2010, 1, 2, 0, 0, 0, tzinfo=timezone.utc),
            datetime(2010, 1, 2, 2, 0, 0, tzinfo=timezone(timedelta(hours=2))),
        )
        for dt in cases:
            with self.subTest(dt):
                self.assertEqual(misc.ensure_tz_utc(dt), datetime(2010, 1, 2, tzinfo=timezone.utc))

    def test_key_value_list_to_dict(self):
        """Test key_value_list_to_dict."""
        data = [
            {'key': 'key_1', 'value': 'value_1'},
            {'key': 'key_2', 'value': 'value_2'},

        ]

        self.assertEqual(
            misc.key_value_list_to_dict(data),
            {
                'key_1': 'value_1',
                'key_2': 'value_2',
            }
        )


class TestDeploymentEnvironment(unittest.TestCase):
    """Test cases for deployment_environment."""

    def test_is_production(self) -> None:
        """Check that functions parsing CKI_DEPLOYMENT_ENVIRONMENT behave correctly."""
        cases = (
            (None, False, False, False, 'development'),
            ('staging', False, True, True, 'staging'),
            ('staging-1', False, True, True, 'staging-1'),
            ('production', True, False, True, 'production'),
            ('production-1', True, False, True, 'production-1'),
            ('something', False, False, False, 'something'),
        )
        for (cki_environment_env,
             is_production, is_staging, is_production_or_staging, environment) in cases:
            patch = {}
            if cki_environment_env:
                patch['CKI_DEPLOYMENT_ENVIRONMENT'] = cki_environment_env
            with self.subTest(cki_environment_env), mock.patch.dict(os.environ, patch):
                self.assertEqual(misc.is_production(), is_production)
                self.assertEqual(misc.is_staging(), is_staging)
                self.assertEqual(misc.is_production_or_staging(), is_production_or_staging)
                self.assertEqual(misc.deployment_environment(), environment)


class TestBoolStr(unittest.TestCase):
    """Test cases for booltostr and strtobool."""

    def test_bool_str(self):
        """Check conversion for booltostr."""
        self.assertEqual(misc.booltostr(False), 'false')
        self.assertEqual(misc.booltostr(None), 'false')
        self.assertEqual(misc.booltostr(''), 'false')
        self.assertEqual(misc.booltostr([]), 'false')
        self.assertEqual(misc.booltostr(True), 'true')
        self.assertEqual(misc.booltostr('str'), 'true')
        self.assertEqual(misc.booltostr(['array']), 'true')

    def test_str_bool(self):
        """Check conversion for strtobool."""
        self.assertIs(misc.strtobool('False'), False)
        self.assertIs(misc.strtobool('false'), False)
        self.assertIs(misc.strtobool('True'), True)
        self.assertIs(misc.strtobool('true'), True)

        with self.assertRaises(ValueError):
            misc.strtobool('1')


class TestBoolEnv(unittest.TestCase):
    """Test the behavior of get_env_bool."""
    @staticmethod
    def _test(envs, *args):
        with mock.patch.dict(os.environ, envs):
            return misc.get_env_bool('UT_BOOL_ENV', *args)

    def test_get_env_bool_true1(self):
        """Check reading a True environment variable."""
        self.assertIs(self._test({'UT_BOOL_ENV': 'True'}),
                      True)

    def test_get_env_bool_true2(self):
        """Check reading a true environment variable."""
        self.assertIs(self._test({'UT_BOOL_ENV': 'true'}),
                      True)

    def test_get_env_bool_true_default(self):
        """Check reading a default True environment variable."""
        self.assertIs(self._test({}, True),
                      True)

    def test_get_env_bool_false1(self):
        """Check reading a False environment variable."""
        self.assertIs(self._test({'UT_BOOL_ENV': 'False'}),
                      False)

    def test_get_env_bool_false2(self):
        """Check reading a false environment variable."""
        self.assertIs(self._test({'UT_BOOL_ENV': 'false'}),
                      False)

    def test_get_env_bool_false_default(self):
        """Check reading a false environment variable."""
        self.assertIs(self._test({}),
                      False)

    def test_get_env_bool_invalid(self):
        """Check reading a non-bool environment variable."""
        self.assertRaises(Exception,
                          lambda: self._test({'UT_BOOL_ENV', 'blub'}))


class TestIntEnv(unittest.TestCase):
    """Test the behavior of get_env_int."""
    @staticmethod
    def _test(envs, *args):
        with mock.patch.dict(os.environ, envs):
            return misc.get_env_int('UT_INT_ENV', *args)

    def test_get_env_int(self):
        """Check reading an int environment variable."""
        self.assertEqual(self._test({'UT_INT_ENV': '1'}, 2), 1)

    def test_get_env_int_default(self):
        """Check reading an int env variable with a string default."""
        self.assertEqual(self._test({}, 2), 2)

    def test_get_env_int_string_default(self):
        """Check reading an int env variable with a string default."""
        self.assertEqual(self._test({}, '2'), 2)

    def test_get_env_int_invalid(self):
        """Check reading a non-bool environment variable."""
        self.assertRaises(Exception,
                          lambda: self._test({'UT_INT_ENV', 'blub'}, 2))


class TestArguments(unittest.TestCase):
    """Test cases for argument parsing."""

    def test_store_name_value_pair(self):
        """Verify key=value argument parsing."""
        parser = argparse.ArgumentParser()
        parser.add_argument('--vars', action=misc.StoreNameValuePair,
                            default={})
        args = parser.parse_args('--vars key=value --vars key2=value2'.split())
        self.assertEqual(args.vars, {'key': 'value', 'key2': 'value2'})

    def test_store_name_value_pair_nargs(self):
        """Verify key=value argument parsing with nargs=+."""
        parser = argparse.ArgumentParser()
        parser.add_argument('--vars', action=misc.StoreNameValuePair,
                            nargs='+', default={})
        args = parser.parse_args('--vars key=value key2=value2'.split())
        self.assertEqual(args.vars, {'key': 'value', 'key2': 'value2'})

    def test_store_name_value_pair_equal(self):
        """Verify key=value argument parsing with equal signs."""
        parser = argparse.ArgumentParser()
        parser.add_argument('--vars', action=misc.StoreNameValuePair,
                            default={})
        args = parser.parse_args('--vars key=value=value2'.split())
        self.assertEqual(args.vars, {'key': 'value=value2'})

    def test_store_name_value_pair_no_default(self):
        """Verify key=value argument parsing without default."""
        parser = argparse.ArgumentParser()
        parser.add_argument('--vars', action=misc.StoreNameValuePair)
        args = parser.parse_args('--vars key=value'.split())
        self.assertEqual(args.vars, {'key': 'value'})

    def test_store_timedelta(self) -> None:
        """Verify timedelta argument parsing."""
        testcases = [
            ({}, '', None),
            ({}, '--delta 1', timedelta(seconds=1)),
            ({}, '--delta 1m', timedelta(minutes=1)),
            ({'nargs': '+'}, '--delta 1 2', [timedelta(seconds=1), timedelta(seconds=2)]),
            ({'default': '1m'}, '', timedelta(minutes=1)),
            ({'default': timedelta(minutes=1)}, '', timedelta(minutes=1)),
        ]
        for kwargs, args, result in testcases:
            parser = argparse.ArgumentParser()
            parser.add_argument('--delta', type=misc.parse_timedelta, **kwargs)
            self.assertEqual(parser.parse_args(args.split()).delta, result)


class TestSentryInit(unittest.TestCase):
    """Test cases sentry_init."""

    @mock.patch.dict(os.environ, {'CKI_DEPLOYMENT_ENVIRONMENT': 'production',
                                  'SENTRY_DSN': 'foo'})
    def test_production(self):
        """Test production call."""
        sentry_sdk = mock.Mock()
        misc.sentry_init(sentry_sdk)

        self.assertTrue(sentry_sdk.init.called)
        sentry_sdk.init.assert_called_with(
            ca_certs=mock.ANY, environment='production'
        )

    @mock.patch.dict(os.environ, {'CKI_DEPLOYMENT_ENVIRONMENT': 'production'})
    def test_no_sentry_dsn(self):
        """Test sentry is skipped if no SENTRY_DSN variable is defined."""
        sentry_sdk = mock.Mock()
        misc.sentry_init(sentry_sdk)

        self.assertFalse(sentry_sdk.init.called)

    @mock.patch.dict(os.environ, {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging',
                                  'SENTRY_DSN': 'foo'})
    def test_staging(self):
        """Test staging call."""
        sentry_sdk = mock.Mock()
        misc.sentry_init(sentry_sdk)

        self.assertTrue(sentry_sdk.init.called)
        sentry_sdk.init.assert_called_with(
            ca_certs=mock.ANY, environment='staging'
        )

    @mock.patch.dict(os.environ, {'CI_JOB_URL': 'https://server/group/project/-/jobs/1',
                                  'SENTRY_DSN': 'foo'})
    def test_pipeline(self):
        """Test that the server name is set to the job url for pipeline runs."""
        sentry_sdk = mock.Mock()
        misc.sentry_init(sentry_sdk)

        self.assertTrue(sentry_sdk.init.called)
        sentry_sdk.init.assert_called_with(
            ca_certs=mock.ANY, environment='development',
            server_name=os.environ['CI_JOB_URL'],
        )

    @mock.patch.dict(os.environ, {'REQUESTS_CA_BUNDLE': '/a/b',
                                  'SENTRY_DSN': 'foo'})
    def test_requests_ca_bundle(self):
        """Test REQUESTS_CA_BUNDLE parameter."""
        sentry_sdk = mock.Mock()
        misc.sentry_init(sentry_sdk)

        self.assertTrue(sentry_sdk.init.called)
        sentry_sdk.init.assert_called_with(
            ca_certs='/a/b',
            environment='development'
        )

    @mock.patch.dict(os.environ, {'SENTRY_DSN': 'foo'})
    def test_params(self):
        """Test call with extra params."""
        sentry_sdk = mock.Mock()
        misc.sentry_init(sentry_sdk, foo='bar')

        self.assertTrue(sentry_sdk.init.called)
        sentry_sdk.init.assert_called_with(
            ca_certs=mock.ANY, environment='development', foo='bar'
        )

    @mock.patch.dict(os.environ, {'SENTRY_DSN': 'foo'})
    def test_params_duplicated(self):
        """Test call with extra params to overrides default ones."""
        sentry_sdk = mock.Mock()
        misc.sentry_init(sentry_sdk, foo='bar', ca_certs='/foo/bar', environment='baar')

        self.assertTrue(sentry_sdk.init.called)
        sentry_sdk.init.assert_called_with(
            ca_certs='/foo/bar', environment='baar', foo='bar'
        )


class TestSentryFlush(unittest.TestCase):
    """Test cases for sentry_flush."""

    @staticmethod
    @mock.patch.dict(sys.modules, {'sentry_sdk': mock.MagicMock()})
    def test_module_exists():
        """Test calling when sentry_sdk is available."""
        misc.sentry_flush()

    @staticmethod
    @mock.patch.dict(sys.modules, {'sentry_sdk': None})
    def test_module_doesnt_exist():
        """Test calling when sentry_sdk is not available."""
        misc.sentry_flush()

    @staticmethod
    @mock.patch.dict(sys.modules, {'sentry_sdk': mock.MagicMock()})
    def test_client_is_none():
        """Test calling when sentry_sdk client is None."""
        sys.modules['sentry_sdk'].Hub.current.client = None
        misc.sentry_flush()


class TestTimeDelta(unittest.TestCase):
    """Test cases for parse_timedelta and related."""

    def test_various(self) -> None:
        """Test various patterns."""
        testcases = [
            ('', None),
            (' ', None),
            ('1foo', None),
            ('1w', timedelta(weeks=1)),
            ('1week', timedelta(weeks=1)),
            ('1.5week', timedelta(weeks=1.5)),
            ('1 week', timedelta(weeks=1)),
            ('2weeks', timedelta(weeks=2)),
            ('1d', timedelta(days=1)),
            ('1day', timedelta(days=1)),
            ('1.5day', timedelta(days=1.5)),
            ('1 day', timedelta(days=1)),
            ('2days', timedelta(days=2)),
            ('1h', timedelta(hours=1)),
            ('1hour', timedelta(hours=1)),
            ('1.5hour', timedelta(hours=1.5)),
            ('1 hours', timedelta(hours=1)),
            ('2hours', timedelta(hours=2)),
            ('1m', timedelta(minutes=1)),
            ('1min', timedelta(minutes=1)),
            ('1.5min', timedelta(minutes=1.5)),
            ('1mins', timedelta(minutes=1)),
            ('1 mins', timedelta(minutes=1)),
            ('2mins', timedelta(minutes=2)),
            ('1minute', timedelta(minutes=1)),
            ('1minutes', timedelta(minutes=1)),
            ('1', timedelta(seconds=1)),
            ('1s', timedelta(seconds=1)),
            ('1sec', timedelta(seconds=1)),
            ('1.5sec', timedelta(seconds=1.5)),
            ('1secs', timedelta(seconds=1)),
            ('1 secs', timedelta(seconds=1)),
            ('2secs', timedelta(seconds=2)),
            ('1second', timedelta(seconds=1)),
            ('1seconds', timedelta(seconds=1)),
            ('5m 30', timedelta(minutes=5, seconds=30)),
            ('5m30', timedelta(minutes=5, seconds=30)),
            ('5m30s', timedelta(minutes=5, seconds=30)),
        ]
        for pattern, result in testcases:
            if result is None:
                self.assertRaises(Exception, misc.parse_timedelta, pattern)
            else:
                self.assertEqual(misc.parse_timedelta(pattern), result)

    @mock.patch("cki_lib.kcidb.validate.sanitize_kcidb_status")
    def test_sanitize_kcidb_subtest_status(self, mocked_sanitize):
        """Check sanitize_kcidb_subtest_status function warns about deprecation and still works."""
        mocked_sanitize.return_value = mock.sentinel.output

        with self.assertWarns(DeprecationWarning):
            result = misc.sanitize_kcidb_subtest_status(mock.sentinel.input)

        self.assertEqual(result, mock.sentinel.output)
        mocked_sanitize.assert_called_once_with(mock.sentinel.input)


class TestBeakerFetchUrlToWebUrl(unittest.TestCase):
    """Check beaker_fetch_url_to_web_url."""

    def test_beaker_fetch_url_to_web_url(self) -> None:
        """Check beaker_fetch_url_to_web_url."""
        testcases = [
            ('default',
             'https://gitlab.com/cki-project/cki-lib/-/archive/main/cki-lib-main.zip#tests/kcidb',
             'https://gitlab.com/cki-project/cki-lib/-/tree/main/tests/kcidb'),
            ('zip',
             'https://gitlab.com/cki-project/cki-lib/-/archive/main/cki-lib-main.zip',
             'https://gitlab.com/cki-project/cki-lib/-/tree/main/'),
            ('tar.gz',
             'https://gitlab.com/cki-project/cki-lib/-/archive/main/cki-lib-main.tar.gz',
             'https://gitlab.com/cki-project/cki-lib/-/tree/main/'),
            ('tar.bz2',
             'https://gitlab.com/cki-project/cki-lib/-/archive/main/cki-lib-main.tar.bz2',
             'https://gitlab.com/cki-project/cki-lib/-/tree/main/'),
            ('tar',
             'https://gitlab.com/cki-project/cki-lib/-/archive/main/cki-lib-main.tar',
             'https://gitlab.com/cki-project/cki-lib/-/tree/main/'),
            ('unknown',
             'https://gitlab.com/cki-project/cki-lib/-/pipelines/1336762391',
             'https://gitlab.com/cki-project/cki-lib/-/pipelines/1336762391'),
            ('gerrit',
             'git://gerrit.com/repo.git?main#general/kpatch',
             'https://gerrit.com/git/repo.git/tree/general/kpatch?h=main'),
            ('gerrit branch',
             'git://gerrit.com/repo.git#general/kpatch',
             'https://gerrit.com/git/repo.git/tree/general/kpatch'),
            ('github',
             'https://github.com/beaker-project/beaker-core-tasks/archive/master.zip',
             'https://github.com/beaker-project/beaker-core-tasks/tree/master/'),
            ('github tar.gz',
             'https://github.com/beaker-project/beaker-core-tasks/archive/master.tar.gz',
             'https://github.com/beaker-project/beaker-core-tasks/tree/master/'),
            ('github refs/heads',
             'https://github.com/beaker-project/beaker-core-tasks/archive/refs/heads/master.zip',
             'https://github.com/beaker-project/beaker-core-tasks/tree/master/'),
            ('github dir',
             'https://github.com/beaker-project/beaker-core-tasks/archive/master.zip#check-install',
             'https://github.com/beaker-project/beaker-core-tasks/tree/master/check-install'),
            ('github other',
             'https://github.com/kernelci/kcidb-io/releases',
             'https://github.com/kernelci/kcidb-io/releases'),
            ('fallback',
             'https://pooh/bear',
             'https://pooh/bear'),
            ('lookaside',
             'https://s3.amazonaws.com/arr-cki-prod-lookaside/lookaside/'
             'kernel-tests-public/kernel-tests-production.zip#check-install',
             'https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/'
             '-/tree/production/check-install'),
        ]
        for description, fetch_url, expected in testcases:
            with self.subTest(description):
                self.assertEqual(misc.beaker_fetch_url_to_web_url(fetch_url), expected)
