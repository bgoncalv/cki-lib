"""Tests for remote-responses image."""
import os

from cki_lib import session
from cki_lib.inttests import cluster
from cki_lib.inttests.remote_responses import RemoteResponsesServer

SESSION = session.get_session(__name__)


@cluster.skip_without_requirements()
class TestRemoteResponses(RemoteResponsesServer):
    """Tests for remote-responses image."""

    image_under_test = 'remote-responses'
    url: str

    data1 = {'a': 'b'}
    data2 = {'a': 'c'}

    def setUp(self) -> None:
        """Provide a shortcut to the remote response server URL."""
        super().setUp()
        self.url = os.environ['REMOTE_RESPONSES_URL_EXTERNAL']

    def test_post(self) -> None:
        """Test a mocked post request."""
        data = {'data': {'namespace': {'fullPath': 'foo', 'rootStorageStatistics': {
            'repositorySize': 1}, 'projects': {'nodes': []}, }}}
        self.responses.add('POST', f'{self.url}/api/graphql', json=data, match=[
            self.responses.json_params_matcher({"operationName": "pooh"}, strict_match=False)])
        response = SESSION.post(f'{self.url}/api/graphql', json={'operationName': 'pooh'})
        self.assertEqual(response.json(), data)

    def test_add(self) -> None:
        """Test responses.add."""
        self.responses.add('GET', f'{self.url}/pooh', json=self.data1)
        self.responses.add('GET', f'{self.url}/pooh', json=self.data2)
        self.assertEqual(SESSION.get(f'{self.url}/pooh').json(), self.data1)
        self.assertEqual(SESSION.get(f'{self.url}/pooh').json(), self.data2)

    def test_upsert_add(self) -> None:
        """Test responses.upsert."""
        self.responses.upsert('GET', f'{self.url}/pooh', json=self.data1)
        self.assertEqual(SESSION.get(f'{self.url}/pooh').json(), self.data1)

    def test_upsert_replace(self) -> None:
        """Test responses.upsert."""
        self.responses.add('GET', f'{self.url}/pooh', json=self.data1)
        self.responses.upsert('GET', f'{self.url}/pooh', json=self.data2)
        self.assertEqual(SESSION.get(f'{self.url}/pooh').json(), self.data2)

    def test_replace(self) -> None:
        """Test responses.replace."""
        self.responses.add('GET', f'{self.url}/pooh', json=self.data1)
        self.responses.replace('GET', f'{self.url}/pooh', json=self.data2)
        self.assertEqual(SESSION.get(f'{self.url}/pooh').json(), self.data2)

    def test_calls(self) -> None:
        """Test responses.calls."""
        self.responses.add('GET', f'{self.url}/pooh', json=self.data1)
        self.responses.add('GET', f'{self.url}/bear', json=self.data2)
        self.assertEqual(len(self.responses.calls), 0)
        self.assertEqual(SESSION.get(f'{self.url}/pooh').json(), self.data1)
        self.assertEqual(SESSION.get(f'{self.url}/bear').json(), self.data2)
        self.assertEqual(SESSION.get(f'{self.url}/bear').json(), self.data2)
        self.assertEqual(len(self.responses.calls), 3)

    def test_responses(self) -> None:
        """Test responses.BaseResponse."""
        r1 = self.responses.add('GET', f'{self.url}/pooh', json=self.data1)
        r2 = self.responses.add('GET', f'{self.url}/bear', json=self.data2)
        self.assertEqual(len(r1.calls), 0)
        self.assertEqual(r1.call_count, 0)
        self.assertEqual(len(r2.calls), 0)
        self.assertEqual(r2.call_count, 0)
        self.assertEqual(SESSION.get(f'{self.url}/pooh').json(), self.data1)
        self.assertEqual(len(r1.calls), 1)
        self.assertEqual(r1.call_count, 1)
        self.assertEqual(len(r2.calls), 0)
        self.assertEqual(r2.call_count, 0)
