"""Tests for HV inttest provider."""
import os

import requests

from cki_lib.inttests import cluster
from cki_lib.inttests.vault import HashiCorpVaultServer
from cki_lib.session import get_session

SESSION = get_session(__name__, raise_for_status=True)


@cluster.skip_without_requirements()
class TestHashiCorpVaultSecrets(HashiCorpVaultServer):
    """Tests for HV inttest provider."""

    def test_api(self) -> None:
        """Test accessing the API."""
        vault_addr = os.environ['VAULT_ADDR']
        vault_mount_point = os.environ['VAULT_MOUNT_POINT']
        url = f'{vault_addr}/v1/{vault_mount_point}/data/foo'
        headers = {'X-Vault-Token': os.environ['VAULT_TOKEN']}
        data = {'value': 'bar'}

        with self.assertRaises(requests.exceptions.HTTPError):
            SESSION.get(url, headers=headers)
        SESSION.post(url, headers=headers, json={'data': data})
        self.assertEqual(SESSION.get(url, headers=headers).json()['data']['data'], data)
